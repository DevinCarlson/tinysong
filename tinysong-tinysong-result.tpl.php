<?php

/**
 * @file
 * Default theme implementation for displaying a banner.
 *
 * A banner wraps slides in HTML, which provides an anchor for the Nivo Slider
 * to target and create a slideshow with the appropriate settings and theming.
 *
 * Available variables:
 * - $url: String containing the name of the currently active theme.
 * - $song_id: String of HTML representing a banner.
 * - $song_name: String of HTML representing any HTML captions.
 * - $artist_id: String of HTML representing any HTML captions.
 * - $artist_name: String of HTML representing any HTML captions.
 * - $album_id: String of HTML representing any HTML captions.
 * - $album_name: String of HTML representing any HTML captions.
 *
 * @see template_preprocess()
 * @see template_process()
 */
?>

<div class="song">
  Song Name: <a href="<?php print $url; ?>"><?php print $song_name; ?></a><br />
  Artist Name: <?php print $artist_name; ?><br />
  Album Name: <?php print $album_name; ?><br />

  <object width="250" height="40">
    <param name="movie" value="http://grooveshark.com/songWidget.swf" />
    <param name="wmode" value="window" />
    <param name="allowScriptAccess" value="always" />
    <param name="flashvars" value="hostname=cowbell.grooveshark.com&songIDs=<?php print $song_id; ?>&style=metal&p=0" />
    <embed src="http://grooveshark.com/songWidget.swf" type="application/x-shockwave-flash" width="250" height="40" flashvars="hostname=cowbell.grooveshark.com&songIDs=<?php print $song_id; ?>&style=metal&p=0" allowScriptAccess="always" wmode="window" />
  </object>
</div>