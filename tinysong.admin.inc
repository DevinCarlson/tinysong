<?php
/**
 * @file
 * Generate configuration form and save settings.
 */

/**
 * Implements hook_form().
 */
function tinysong_configuration_form($form, &$form_state) {
  $form['tinysong_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('tinysong_api_key', ''),
    '#description' => t('Enter your Tinysong <a href="@api">API key</a>.', array('@api' => 'http://tinysong.com/api')),
  );
  $form['tinysong_api_method'] = array(
    '#type' => 'radios',
    '#title' => t('Search results'),
    '#options' => array(
      'a' => t('A single Tinysong link'),
      'b' => t('A single Tinysong link with additional meta-information'),
      's' => t('Multiple Tinysong links with additional meta-information'),
    ),
    '#default_value' => variable_get('tinysong_api_method', 'a'),
    '#description' => t('Choose how search results should be displayed. Tinysong can display search results in three different ways.'),
    '#states' => array(
      'visible' => array(
        ':input[name="tinysong_api_key"]' => array('empty' => FALSE),
      ),
    ),
  );
  $form['tinysong_result_limit'] = array(
    '#type' => 'select',
    '#title' => t('Result limit'),
    '#options' => drupal_map_assoc(array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32)),
    '#default_value' => variable_get('tinysong_result_limit', 5),
    '#description' => t('Select a result limit. Tinysong can display a maximum of 32 results.'),
    '#states' => array(
      'visible' => array(
        ':input[name="tinysong_api_key"]' => array('empty' => FALSE),
        ':input[name="tinysong_api_method"]' => array('value' => 's'),
      ),
    ),
  );

  return system_settings_form($form);
}
